// settingan
var express     = require('express');
var bodyParser  = require('body-parser');
var app         = express();
var router      = express.Router();
var port        = process.env.PORT || 3000;

// connnect database mongoose
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/apiusers');

var User = require('./models/dataUsers')

// Konfigurasi Body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// middleware
router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

// url testing
router.get('/', (req, res) => {
    res.json({ massage: 'anda berada di Home' });
})

router.route('/users')

    .post((req, res) => {
        var user        = new User();
        user.username   = req.body.username;
        user.age        = req.body.age;
        user.address    = req.body.address;
        user.status     = req.body.status;

        user.save(err => {
            if(err) res.send(err);

            res.json({ massage: "user berhasil di tambahkan" })
        })
}).get((req, res) => {
    User.find((err, users) => {
        if(err) res.send(err);

        res.json(users)
    })
})

// Url Prefix
app.use('/api', router)

// Listen Port
app.listen(port);
console.log(`silahkan running di Port...${port}`)