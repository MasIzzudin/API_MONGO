var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: {
        type: String,
    },
    age: {
        type: Number,
    },
    address: {
        type: String
    },
    status: {
        type: [{
            type: String,
            enum: ['Lajang', 'Menikah']
        }],
        default: ['Lajang']
    }
});

module.exports = mongoose.model('User', userSchema)